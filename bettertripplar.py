
from time import time, sleep
from itertools import product
import math, random, multiprocessing, threading, sys

shouldiprint = True

start=1
end=30000
cores=4
doshortcut=False
shortcut_end=end+1000

#Shortcut to generating pythagorean tripples from known triples.
def shortcut(a,b,c,end,sju, short_end):
    a, b, c = a*end, b*end, c*end
    for count in range(end,short_end):
        a, b, c = a + a , b + b, c + c
        sju+=1
        if sju%(end**3)==0:
            print(f'{a} {b} {c}')
    return sju

#Generating pythagorean triples using Euclid's formula.
def pytripples(cores, start, end, p, doshortcut, shortcut_end):
    sju=0
    print(f'started worker: {p}')  
    for n in range(start+p,end, cores):
        for m in range(n+1,end):
            a=m*m-n*n
            b=2*m*n
            c=m*m+n*n
            sju+=1
            if sju%(end*2000)==0:
                print(f'{a} {b} {c}')
            if doshortcut==True:
                sju=shortcut(a,b,c,end,sju, shortcut_end)
    return sju

#Sum all values of sju.
def sum(list):
    total=0
    for v in list:
        total+=v
    return total

if __name__=='__main__':
    
    print() 
    if shouldiprint == True:
        print(f'start = {start}\nend = {end}\ncores = {cores}\ndoshortcut = {doshortcut}\nshortend = {shortcut_end}\n')
    
    stime=time()

    acores, startpos, endpos, a_doshortcut, a_shortcut_end, p = [], [], [], [], [], []
    for i in range(cores):
        acores.append(cores)
        startpos.append(start)
        endpos.append(end)
        p.append(i)
        a_doshortcut.append(doshortcut)
        a_shortcut_end.append(shortcut_end)

    pool=multiprocessing.Pool(processes=cores)
    result=pool.starmap(pytripples, zip(acores, startpos, endpos, p, a_doshortcut, a_shortcut_end))
    pool.terminate()
    
    worktime=time()-stime
    sju=sum(result)
    print(f'\n{sju} tripplar hittades.\n{worktime} sekunder\n{sju/worktime} t/s')
